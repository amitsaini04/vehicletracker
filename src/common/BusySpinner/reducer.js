import * as types from './actionTypes';

const initialState = {
  count: 0
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case types.INCREMENT_BUSY_COUNT:
      return Object.assign(
        {}, state, { count: state.count + 1 });

    case types.DECREMENT_BUSY_COUNT:
      return Object.assign(
        {}, state, { count: state.count - 1 });

    default:
      return state;
  }
}

