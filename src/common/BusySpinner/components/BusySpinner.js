import React from 'react';
import {
  StyleSheet,
  Dimensions
} from 'react-native';
import {
  View,
  Spinner
} from 'native-base';
import styles from './styles'

const BusySpinner = ({ show, children}) => {
  return(
    <View style={styles.flex1}>
      {show ?
        <View style={styles.spinnerContainer}>
          <Spinner color={styles.spinnerColor.color} />
        </View>:
        <View style={styles.flex1}>
        {children}  
        </View> 
    }
    </View>
  )
}


export default BusySpinner;
