import { StyleSheet } from 'react-native'

const Styles = StyleSheet.create({
    flex1: {
        flex: 1
    },
    spinnerContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    spinnerColor:{
        color:'#141D2D'
    }
})

export default Styles