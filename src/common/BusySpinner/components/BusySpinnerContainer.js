import React from 'react';
import { connect } from 'react-redux';
import * as constants from '../constants';
import BusySpinner from './BusySpinner';

class BusySpinnerContainer extends React.Component {

  constructor(props, context) {
    super(props, context);
  }

  render() {
    const { show, children } = this.props
      return(
        <BusySpinner show={show} children={children} />
    );
  }
}


function mapStateToProps(state) {
  return {
    show: state[constants.NAME].count > 0
  };
}

export default connect(mapStateToProps)(BusySpinnerContainer);
