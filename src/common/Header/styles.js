import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    left: {
        flex: 0.3,
    },
    body: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        alignSelf: 'center'
    },
    right: {
        flex: 0.3
    }
})

export default styles