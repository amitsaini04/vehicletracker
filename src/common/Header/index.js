import React, { Component } from 'react';
import { Header, Left, Body, Right, Button, Icon, Title } from 'native-base';
import styles from './styles'

export default class CustomHeader extends Component {

    render() {
        const { goBack, customRightButton } = this.props;
        return (
            <Header>
                <Left style={styles.left}>
                    {goBack ?
                        <Button onPress={() => goBack()}  transparent>
                            <Icon name='arrow-back' />
                        </Button> : null}
                </Left>
                <Body style={styles.body}>
                    <Title style={styles.title}>{this.props.title}</Title>
                </Body>
                <Right style={styles.right}>{customRightButton}</Right>
            </Header>
        );
    }
}