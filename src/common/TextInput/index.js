import React, { Component } from 'react';
import { Item, Input } from 'native-base';
export default class TextInput extends Component {
    render() {
        const inputProps = this.props.input
        return (
          <Item>
            <Input {...inputProps} {...this.props}/>
          </Item>
        )
    }
}