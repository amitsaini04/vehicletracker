import { AsyncStorage } from 'react-native'
import * as constants from './constants'
export async function AddAccessToken(accessToken) {
    try {
        await AsyncStorage.setItem(constants.ACCESS_TOKEN, accessToken)
    }
    catch (error) {
    }
}

export async function RemoveAccessToken(cb) {
    try {
        await AsyncStorage.removeItem(constants.ACCESS_TOKEN)
        cb()
    }
    catch (error) {
        console.log(error)
        cb()
    }
}

export async function GetAccessToken(cb) {
    try {
        let token = await AsyncStorage.getItem(constants.ACCESS_TOKEN)
        cb(token)
    }
    catch (error) {
        cb()
    }
}
