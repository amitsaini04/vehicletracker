import VehicleLocation from '../screens/vehicleLocation/components'
import Vehicle from '../screens/vehicle/components'
import Login from '../screens/login/components'
import FTU from '../screens/ftu/components'
import routes from './routes'
const stackRoutes = {
    [routes.FTU]:{screen:FTU},
    [routes.Vehicle]:{screen:Vehicle},
    [routes.Login]:{screen:Login},
    [routes.Location]:{screen:VehicleLocation}
};

export default stackRoutes;