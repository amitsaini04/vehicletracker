import { createStackNavigator } from 'react-navigation'
import stackRouter from './stackRouter'
const GlobalNavigation = createStackNavigator(stackRouter, {
    headerMode: 'none',
    navigationOptions: {
        headerVisible: false,
    }
})
export default GlobalNavigation