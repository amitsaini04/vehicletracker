import React from 'react'
import { View, TouchableOpacity } from 'react-native'
import * as constants from '../constants'
import styles from './styles'
import { StyleProvider, Container, Button, Text, Content } from 'native-base';
import getTheme from '../../../../themes/components';
import commonColor from '../../../../themes/variables/commonColor';
import CustomHeaders from '../../../common/Header'
import TextInput from '../../../common/TextInput'
import { Field, reduxForm } from 'redux-form';
import BusySpinner from '../../../common/BusySpinner/components/BusySpinnerContainer';

const LoginRender = ({ handleSubmit, loginUser }) => {
  return (
    <StyleProvider style={getTheme(commonColor)}>
      <Container style={styles.container}>
        <CustomHeaders title="Login" />
        <BusySpinner>
          <View style={styles.loginContainer}>
            <View style={styles.inputContainer}>
              <Field name='username' component={TextInput} placeholder="Username" style={styles.input} />
              <Field name='password' component={TextInput} placeholder="Password" style={styles.input} secureTextEntry />
            </View>
            <View style={styles.submitButtonContainer}>
              <Button onPress={handleSubmit(loginUser)} block>
                <Text>LOG IN</Text>
              </Button>
            </View>
          </View>
        </BusySpinner>
      </Container>
    </StyleProvider>
  )
}

export default reduxForm({
  form: constants.LoginForm
})(LoginRender)
