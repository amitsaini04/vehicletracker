import { StyleSheet } from 'react-native'

const Styles = StyleSheet.create({
    container: {
        backgroundColor: '#f4f4f4'
    },
    loginContainer: {
        flex: 1,
        marginLeft: 10,
        marginRight: 10
    },
    inputContainer:
    {
        alignItems: "center",
        flex: 1
    },
    input:{
        marginTop: 5,
        marginBottom: 5
    },
    submitButtonContainer:{
        flex: 0.2 
    }
})

export default Styles
