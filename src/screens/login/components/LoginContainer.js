import React from 'react';
import * as actions from '../actions';
import routes from '../../../navigation/routes'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import LoginRender from './LoginRender';

class LoginContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
    }
    this.loginUser = this.loginUser.bind(this)
  }

  componentDidMount() {

  }

  loginUser(formValues) {
    console.log(formValues)
    if (!formValues.password || !formValues.username) {
      var errorString = "Please enter"
      if (!formValues.username) {
        errorString = errorString + " username";
      }
      if (!formValues.password) {
        errorString = errorString + (!formValues.username ? " and" : "") + " password";
      }
      alert(errorString)
      return;
    }
    this.props.actions.signin(formValues, () => {
      this.props.navigation.navigate(routes.Vehicle)
    }, () => {
      alert("Invalid username or password")
    })

  }

  render() {
    return (
      <LoginRender loginUser={this.loginUser} />
    );
  }
}

function mapDispatchToProps(dispatch) {
  const actionsToBind = Object.assign({}, actions)
  return {
    actions: bindActionCreators(actionsToBind, dispatch)
  };
}

export default connect(null, mapDispatchToProps)(LoginContainer)
