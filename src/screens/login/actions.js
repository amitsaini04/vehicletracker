import * as actionTypes from './actionTypes'
import * as commonConstants from '../../common/Constants'
import axios from 'axios'
import { AddAccessToken } from '../../common/AsyncStorage'
import busySpinner from '../../common/BusySpinner';

export function accessTokenAction(response) {
    return {
        type: actionTypes.USER_LOGIN,
        payload: response
    }
}

export function signin(data, successCB, errorCB) {
    return (dispatch) => {
        dispatch(busySpinner.actions.incrementBusyCount());
        axios.post(`${commonConstants.ROOT_URL}/login/`, Object.assign({},data,{type:'json'}), { headers: { 'content-type': 'application/json' } }
        ).then(function (response) {
            console.log(response)
            let token = response.data.meta.data.token
            AddAccessToken(token)
            dispatch(accessTokenAction(token))
            dispatch(busySpinner.actions.decrementBusyCount());
            successCB();
        }).catch(function (error) {
            console.log(error)
            dispatch(busySpinner.actions.decrementBusyCount());
            errorCB();
        })
    };
}
