import * as actionTypes from './actionTypes';
const initialState = {
}

export default function reducer(state = initialState, action) {
    switch(action.type){
        case actionTypes.USER_LOGIN:
        return Object.assign({},state,{accessToken:action.payload})
        default: return state;
    }
}
