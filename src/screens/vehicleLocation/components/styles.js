import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    container:{
        backgroundColor: '#f4f4f4'
    },
    mapContainer: {
      flex:1
    },
    map: {
        flex:1
    },
   });

   export default styles