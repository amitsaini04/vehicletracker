import React from 'react'
import { View } from 'react-native'
import styles from './styles'
import { StyleProvider, Container, Button, Text, Content, Card, CardItem, Body, Right, Icon } from 'native-base';
import CustomHeaders from '../../../common/Header'
import getTheme from '../../../../themes/components';
import commonColor from '../../../../themes/variables/commonColor';
import MapView, { Marker } from 'react-native-maps';
class VehicleLocationRender extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
        }
        this.goBack = this.goBack.bind(this)
    }
    goBack() {
        this.props.navigation.goBack();
    }
    static navigationOptions = {
        title: 'Map',
    };
    render() {
        const locationInfo = this.props.navigation.getParam('locationInfo', {});
        return (
            <StyleProvider style={getTheme(commonColor)}>
                <Container style={styles.container}>
                    <CustomHeaders title="Map" goBack={this.goBack} />
                    <View style={styles.mapContainer}>
                        <MapView
                            style={styles.map}
                            region={{
                                latitude: locationInfo.Latitude,
                                longitude: locationInfo.Longitude,
                                latitudeDelta: 1,
                                longitudeDelta: 1
                            }}
                        >
                            <Marker coordinate={{
                                latitude: locationInfo.Latitude,
                                longitude: locationInfo.Longitude,
                            }} />
                        </MapView>
                    </View>
                </Container>
            </StyleProvider>
        )
    }
}

export default VehicleLocationRender