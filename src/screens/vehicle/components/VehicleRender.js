import React from 'react'
import { View, TouchableOpacity, FlatList } from 'react-native'
import styles from './styles'
import { StyleProvider, Container, Button, Text, Icon, Card, CardItem, Body, Right } from 'native-base';
import CustomHeaders from '../../../common/Header'
import getTheme from '../../../../themes/components';
import commonColor from '../../../../themes/variables/commonColor';
import BusySpinner from '../../../common/BusySpinner/components/BusySpinnerContainer';

function renderVehicleData(data, openMap) {
  return (
    <FlatList
      data={data}
      keyExtractor={(item, index) => index + ""}
      renderItem={({ item }) =>
        <Card style={styles.card}>
          <TouchableOpacity onPress={() => openMap(item)}>
            <CardItem header bordered>
              <Body style={styles.body}>
                <Text style={styles.header}>Vehicle No: {item.Vehicle_no} </Text>
              </Body>
              <Right style={styles.right}>
                <Icon name="arrow-forward" />
              </Right>
            </CardItem>
          </TouchableOpacity>
          <CardItem>
            <Body>
              <Text>
                Current Fuel Value:{item.Current_Fuel_Value} {"\n"}
                Distance: {item.Distance} {"\n"}
                Fuel Consumed: {item.Fuel_consume} {"\n"}
                Time: {item.Time} {"\n"}
                Vehicle Type:{item.Vehicle_type} {"\n"}
              </Text>
            </Body>
          </CardItem>
        </ Card>
      }
    />
  );
}

const VehicleRender = ({ openMap, vehiclesData,logOut }) => {
  return (
    <StyleProvider style={getTheme(commonColor)}>
      <Container style={styles.container}>
        <CustomHeaders title="Home" customRightButton={
          <Button onPress={()=>logOut()} transparent>
            <Icon name="ios-log-out" />
          </Button>
        } />
        <BusySpinner>
          {renderVehicleData(vehiclesData, openMap)}
        </BusySpinner>
      </Container>
    </StyleProvider>
  )
}

export default VehicleRender
