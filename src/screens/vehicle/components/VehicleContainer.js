import React from 'react';
import * as AsyncStorage from '../../../common/AsyncStorage'
import * as actions from '../actions';
import { NavigationActions, StackActions } from 'react-navigation'
import routes from '../../../navigation/routes'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as loginConstants from '../../login/constants'
import * as vehicleConstant from '../constants'
import VehicleRender from './VehicleRender';

class VehicleContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
    }
    this.openMap = this.openMap.bind(this);
    this.logOut = this.logOut.bind(this);
  }
  static navigationOptions = {
    title: 'Home',
  };

  componentDidMount() {
    this.props.actions.fetchVehicles(this.props.accessToken, () => { alert("Something went wrong.") })
  }

  openMap(locationInfo) {
    this.props.navigation.navigate(routes.Location, { locationInfo })
  }

  navigateToLogin() {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: routes.Login })],
    });
    this.props.navigation.dispatch(resetAction);
  }

  logOut() {
    AsyncStorage.RemoveAccessToken(() => {
      this.navigateToLogin()
    });

  }

  render() {
    return (
      <VehicleRender openMap={this.openMap} vehiclesData={this.props.vehiclesData}
        logOut={this.logOut} />
    );
  }
}

function mapStateToProps(state) {
  console.log(state)
  return {
    accessToken: state[loginConstants.NAME].accessToken,
    vehiclesData: state[vehicleConstant.NAME].vehiclesData ? state[vehicleConstant.NAME].vehiclesData : []
  }
}

function mapDispatchToProps(dispatch) {
  const actionsToBind = Object.assign({}, actions)
  return {
    actions: bindActionCreators(actionsToBind, dispatch)
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(VehicleContainer)
