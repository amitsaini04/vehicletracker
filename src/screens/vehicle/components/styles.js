import { StyleSheet, Dimensions } from 'react-native'

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const Styles = StyleSheet.create({
    container:{
        backgroundColor: '#f4f4f4'
    },
    card: {
        marginLeft: 3,
        marginRight: 3
    },
    right: {
        alignSelf: "flex-end",
        flex: 0.2
    },
    body: {
        flex: 0.8
    },
    header: {
        color: '#3F5185'
    }
})

export default Styles
