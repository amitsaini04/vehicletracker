import * as actionTypes from './actionTypes'
import * as commonConstants from '../../common/Constants'
import axios from 'axios'
import busySpinner from '../../common/BusySpinner';

function getResponse(response) {
    return {
        type: actionTypes.FETCH_VEHICLES,
        payload: response
    }
}

export function fetchVehicles(accessToken, errorCb) {
    return (dispatch) => {
        dispatch(busySpinner.actions.incrementBusyCount());
        axios.get(`${commonConstants.ROOT_URL}/vehicle/no/`,
            { headers: { 'X-Access-Token': accessToken } }
        ).then(function (response) {
            console.log(response)
            if (!response.data) {
                errorCb();
                return;
            }
            let vehiclesData = response.data.meta.data
            dispatch(getResponse(vehiclesData))
            dispatch(busySpinner.actions.decrementBusyCount());
        }).catch(function (error) {
            errorCb();
            console.log(error)
            dispatch(busySpinner.actions.decrementBusyCount());
        })
    }
}