import * as actionTypes from './actionTypes';
const initialState = {
}

export default function reducer(state = initialState, action) {
console.log(action)
    switch (action.type) {
        case actionTypes.FETCH_VEHICLES:
            return Object.assign({}, state, {vehiclesData:action.payload});
        default: return state;
    }
}
