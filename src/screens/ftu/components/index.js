import React from 'react'
import * as AsyncStorage from '../../../common/AsyncStorage'
import routes from '../../../navigation/routes'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from '../../login/actions'
import { View, Image } from 'react-native'
import styles from './styles'
const GOBOLT_LOGO = require('../../../assets/images/GoBOLT-Logo.jpg')
class FTU extends React.Component {
    constructor(props) {
        super(props)
    }
    componentDidMount() {
        AsyncStorage.GetAccessToken((accessToken) => {
            if (accessToken) {
                this.props.actions.accessTokenAction(accessToken);
                setTimeout(() => {
                    this.props.navigation.navigate(routes.Vehicle)
                }, 1000);
            }
            else {
                setTimeout(() => {
                    this.props.navigation.navigate(routes.Login);
                }, 1000);
            }
        })
    }
    render() {
        return (
            <View style={styles.container}><Image
                resizeMode="stretch" source={GOBOLT_LOGO} style={styles.logo} /></View>
        );
    }
}

function mapDispatchToProps(dispatch) {
    const actionsToBind = Object.assign({}, actions)
    return {
        actions: bindActionCreators(actionsToBind, dispatch)
    };
}


export default connect(null, mapDispatchToProps)(FTU)

