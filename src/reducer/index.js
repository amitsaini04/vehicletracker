import {combineReducers} from 'redux'
import {reducer as formReducer} from 'redux-form'
import login from '../screens/login'
import vehicle from '../screens/vehicle'
import busySpinner from '../common/BusySpinner'

const rootReducer = combineReducers({
    form:formReducer,
    [login.constants.NAME]:login.reducer,
    [vehicle.constants.NAME]:vehicle.reducer,
    [busySpinner.constants.NAME]:busySpinner.reducer
    
});

export default rootReducer