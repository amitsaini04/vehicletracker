/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import promise from 'redux-promise'
import thunk from 'redux-thunk';
import rootReducer from './src/reducer'
import GlobalNavigation from './src/navigation'
const createStoreWithMiddleware = applyMiddleware(promise,thunk)(createStore)

export default class App extends Component {
  render() {
    return (
      <Provider store={createStoreWithMiddleware(rootReducer)}>
        <GlobalNavigation />
      </Provider>
    );
  }
}